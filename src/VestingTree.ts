import dayjs from 'dayjs';
import keccak256 from 'keccak256';
import utc from 'dayjs/plugin/utc';
import { BigNumber } from 'ethers';
import { MerkleTree } from 'merkletreejs';
import { soliditySha3 } from 'web3-utils';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import { parseEther, formatEther, isAddress } from 'ethers/lib/utils';

import { VESTING_CLIFF_DEFAULT } from './constants';

/* types */
import type {
  Allocation,
  VestingUsers,
  TreasurerData,
  GroupedByUsers,
  GroupedByCliff,
  VestingSchedule,
  VestingTreeParams,
  AllocationTypeMapCliff,
  TreasurerDataByAddress,
  AllocationsType,
} from './types';

dayjs.extend(utc);
dayjs.extend(LocalizedFormat);

// Multiplier to prevent underflow for numbers too small to use with BigNumber
const DENOMINATOR = 1_000_000_000;

const allocationTypeOwner: AllocationsType = 'marketing';

function vestingScheduleHash(item: VestingSchedule) {
  if(!item) return '';

  const hash = soliditySha3(
    { t: 'address', v: item.address },
    { t: 'uint256', v: item.amount },
    { t: 'uint256', v: item.vestingCliff },
  )

  return hash;
}

function treasurerHash(data: TreasurerData) {
  const { address, vestingType } = data;

  return soliditySha3(
    { t: 'address', v: address },
    { t: 'uint256', v: parseEther(`${vestingType.unlocking}`).toString() },
    // @ts-ignore
    { t: 'uint256', v: vestingType.monthly.map(el => parseEther(`${el}`).toString()) },
    { t: 'uint256', v: vestingType.months.map(el => parseEther(`${el}`).toString()) },
    { t: 'uint256', v: parseEther(`${vestingType.cliff}`).toString() },
  )
}

function balanceHash(balance: string) {
  return soliditySha3({ t: "uint256", v: balance });
}

function treasurerQuantityHash(quantity: number) {
  return soliditySha3(
    { t: "string", v: 'TREASURER_QUANTITY' },
    { t: "uint256", v: quantity },
  );
}

function allocationQuantityHash(quantity: number) {
  return soliditySha3(
    { t: "string", v: 'ALLOCATION_QUANTITY' },
    { t: "uint256", v: quantity }
  );
}

/**
 * Class reprensenting a Merkle Tree
 * @namespace VestingTree
**/
export class VestingTree extends MerkleTree {
  userNodes: VestingUsers[] = [];
  allocations: Allocation[] = [];
  treasurers: TreasurerData[] = [];
  groupedByUsers: GroupedByUsers = {};
  groupedByCliff: GroupedByCliff = {};
  vestingSchedules: VestingSchedule[] = [];
  allocationTypeMapCliff: AllocationTypeMapCliff = {};
  treasurerDataByAddress: TreasurerDataByAddress = {};

  /**
   * @desc Constructs a Vesting Tree.
   * @param {Object} params - Setting options
  **/
  constructor(params: VestingTreeParams) {
    const { users, allocations, treasurers = [], ownerAddress = null } = params;
    const balance = BigNumber.from(params.balance);

    const vestingStartTimestamp = dayjs.unix(params.vestingStartTimestamp).utc();

    let balanceCount = BigNumber.from('0');

    const leaves: string[] = [];
    const userNodes: VestingUsers[] = [];
    const groupedByUsers: GroupedByUsers = {};
    const groupedByCliff: GroupedByCliff = {};
    const vestingSchedules: VestingSchedule[] = [];
    const allocationTypeMapCliff: AllocationTypeMapCliff = {};
    const treasurerDataByAddress: TreasurerDataByAddress = {};

    for(const key in allocations) {
      // Initializing group by allocation type
      allocationTypeMapCliff[key] = {};
    }

    for(const treasurer of treasurers) {
      treasurerDataByAddress[treasurer.address] = treasurer;
    }

    for(const { address } of users) {
      // Initializing group by user
      groupedByUsers[address] = [];

    }

    for(const user of users) {
      try {
        userNodes.push(user);
        const { allocationsType, address, weight = BigNumber.from(0) } = user;

        /**
         * Amount of tokens to be awarded. IMPORTANT: if this value exists and is greater than
         * zero, it must have priority over the weight (%)
         */
        const _amount = user.amount || BigNumber.from(0);

        const { percentage, vestingInfo } = allocations[allocationsType];

        /**
         * Divide by 1 ETH, which is the pool's percentage multiplier, and then divide again
         * by 1 ETH to obtain the pool balance in human-readable digits instead of
         * Ethereum-format digits (18 decimals).
        **/
        const poolShareBN = percentage.mul(balance).div(parseEther('1')).div(parseEther('1'));

        if(vestingInfo == 'unlocked') {
          let amount = '';

          if(_amount.gt(0)) {
            amount = _amount.toString();
          }

          if(!_amount.gt(0)){
            amount = poolShareBN.mul(weight).toString();
          }

          const _vestingSchedule: VestingSchedule = {
            address,
            amount,
            vestingCliff: VESTING_CLIFF_DEFAULT,
            allocationsType,
          }

          balanceCount = balanceCount.add(amount);

          if(!groupedByCliff.hasOwnProperty(VESTING_CLIFF_DEFAULT)) {
            // Initializing group by cliff
            groupedByCliff[VESTING_CLIFF_DEFAULT] = [];
          }

          if(!allocationTypeMapCliff[allocationsType].hasOwnProperty(VESTING_CLIFF_DEFAULT)) {
            // Initializing group by allocation type and cliff
            allocationTypeMapCliff[allocationsType][VESTING_CLIFF_DEFAULT] = [];
          }

          vestingSchedules.push(_vestingSchedule);
          groupedByUsers[address].push(_vestingSchedule);
          leaves.push(vestingScheduleHash(_vestingSchedule));
          groupedByCliff[VESTING_CLIFF_DEFAULT].push(_vestingSchedule);
          allocationTypeMapCliff[allocationsType][VESTING_CLIFF_DEFAULT].push(_vestingSchedule);
        }

        if(vestingInfo != 'unlocked') {
          const { cliff, monthly, months, unlocking } = vestingInfo;

          if(!groupedByCliff.hasOwnProperty(VESTING_CLIFF_DEFAULT)) {
            // Initializing group by cliff
            groupedByCliff[VESTING_CLIFF_DEFAULT] = [];
          }

          if(!allocationTypeMapCliff[allocationsType].hasOwnProperty(VESTING_CLIFF_DEFAULT)) {
            // Initializing group by allocation type and cliff
            allocationTypeMapCliff[allocationsType][VESTING_CLIFF_DEFAULT] = [];
          }

          let unlockedAmountAtTGE = BigNumber.from(0);

          if(unlocking > 0) {
            let amount = '';

            if(_amount.gt(0)) {
              amount = _amount.mul(unlocking * DENOMINATOR).div(DENOMINATOR).toString();
            }

            if(!_amount.gt(0)){
              let amountPerUser = poolShareBN.mul(unlocking * DENOMINATOR)
                .div(DENOMINATOR)
                .mul(weight);
              amount = amountPerUser.toString();
            }

            const _vestingSchedule: VestingSchedule = {
              address,
              amount,
              vestingCliff: VESTING_CLIFF_DEFAULT,
              allocationsType,
            }

            unlockedAmountAtTGE = BigNumber.from(amount);
            balanceCount = balanceCount.add(amount);

            vestingSchedules.push(_vestingSchedule);
            groupedByUsers[address].push(_vestingSchedule);
            leaves.push(vestingScheduleHash(_vestingSchedule));
            groupedByCliff[VESTING_CLIFF_DEFAULT].push(_vestingSchedule);
            allocationTypeMapCliff[allocationsType][VESTING_CLIFF_DEFAULT].push(_vestingSchedule);
          }

          months.forEach((month, monthIndex) => {
            const prevCycle = (monthIndex > 0) ? months[monthIndex - 1] : 0;

            // Array with the length of the months in which the allocation releases will be made
            [...new Array(month).keys()].forEach((index) => {
              // 1 must be added because the cycle must start at 1 and the index starts at 0.
              const cycle = index + 1;

              let amount = '';
              const _monthly = monthly[monthIndex];

              if(_amount.gt(0)) {
                amount = _amount.mul(_monthly * DENOMINATOR).div(DENOMINATOR).toString();
              } else {
                const amountPerUser = poolShareBN.mul(_monthly * DENOMINATOR).mul(weight);
                amount = amountPerUser.div(DENOMINATOR).toString();
              }

              const vestingCliff = cliff + (
                vestingStartTimestamp
                .add(cycle + prevCycle, 'months')
                .unix() - vestingStartTimestamp.unix()
              );

              const _vestingSchedule = {
                amount,
                address,
                vestingCliff,
                allocationsType,
              }

              balanceCount = balanceCount.add(amount);

              if(!groupedByCliff.hasOwnProperty(vestingCliff)) {
                // Initializing group by cliff
                groupedByCliff[vestingCliff] = [];
              }

              if(!allocationTypeMapCliff[allocationsType].hasOwnProperty(vestingCliff)) {
                // Initializing group by allocation type and cliff
                allocationTypeMapCliff[allocationsType][vestingCliff] = [];
              }

              vestingSchedules.push(_vestingSchedule);
              groupedByUsers[address].push(_vestingSchedule);
              leaves.push(vestingScheduleHash(_vestingSchedule));
              groupedByCliff[vestingCliff].push(_vestingSchedule);
              allocationTypeMapCliff[allocationsType][vestingCliff].push(_vestingSchedule);
            })
          })
        }
      } catch(error) {
        console.log('> Error in VestingTree - constructor:', error);
        break;
      }
    }

    if(balanceCount.gt(balance)) {
      throw new Error(
        `The total balance of the vesting tree is lower when distributed among the ` +
        `beneficiaries.\nAvailable balance: ${formatEther(balance)}\n` +
        `Distributed balance: ${formatEther(balanceCount)}`
      )
    }

    let ownerBalance = BigNumber.from(0);

    if(balance.gt(balanceCount)) {
      if(!isAddress(ownerAddress)) throw new Error('Invalid owner address');

      ownerBalance = balance.sub(balanceCount);

      // To distribute the leftover tokens to the owner.
      leaves.push(vestingScheduleHash({
        vestingCliff: 0,
        address: ownerAddress,
        allocationsType: allocationTypeOwner,
        amount: ownerBalance.toString(),
      }));
    }

    leaves.push(balanceHash(balance.toString()));
    leaves.push(treasurerQuantityHash(treasurers.length));
    leaves.push(allocationQuantityHash(Object.keys(allocations).length));

    for(const { address, vestingType } of treasurers) {
      leaves.push(treasurerHash({
        address,
        vestingType
      }));
    }

    super(leaves, keccak256, { sortPairs: true });

    this.userNodes = userNodes;
    this.treasurers = treasurers;
    this.groupedByCliff = groupedByCliff;
    this.groupedByUsers = groupedByUsers;
    this.vestingSchedules = vestingSchedules;
    this.allocations = Object.values(allocations);
    this.allocationTypeMapCliff = allocationTypeMapCliff;
    this.treasurerDataByAddress = treasurerDataByAddress;
  }

  hash(item: VestingSchedule) {
    return vestingScheduleHash(item);
  }

  balancehash(item: BigNumber) {
    return balanceHash(item.toString());
  }

  treasurerHash(data: TreasurerData) {
    return treasurerHash(data);
  }

  treasurerQuantityHash() {
    return treasurerQuantityHash(this.treasurers.length);
  }

  allocationQuantityHash() {
    return allocationQuantityHash(this.allocations.length);
  }

  getCliffs() {
    const _cliff: number[] = [];
    const mappingCliff: { [cliff: string]: boolean } = {};

    this.vestingSchedules.forEach((vestingSchdule) => {
      const { vestingCliff } = vestingSchdule;

      if(!mappingCliff[vestingCliff]) {
        _cliff.push(vestingCliff);
        mappingCliff[vestingCliff] = true;
      }
    });

    return _cliff.sort((a, b) => {
      if(a < b) return -1;
      if(a > b) return 1;
      return 0;
    })
  }
}