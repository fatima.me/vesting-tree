import dayjs from 'dayjs';
import { BigNumber } from 'ethers';
import { parseEther } from 'ethers/lib/utils';

import { VestingTree } from '../VestingTree';

/* types */
import {
  Allocations,
  PoolsSupply,
  VestingTypes,
  VestingUsers,
  TreasurerData,
  VestingType
} from '../types';

const VESTING_START_TIMESTAMP = dayjs('04/12/2022 19:00:00');

const VESTING_TYPES: VestingTypes = {
  unlocked: 'unlocked',
  type1: {
    unlocking: 0.12, // 12%
    monthly: [0.08], // 8% monthly over 11 months
    months: [11],
    cliff: 0,
    label: '1'
  },
  type2: {
    unlocking: 0.1, // 10%
    monthly: [0.06], // 6% monthly over 15 months
    months: [15],
    cliff: 0,
    label: '2'
  },
  type3: {
    unlocking: 0.1, // 10%
    monthly: [0.05], // 5% monthly over 18 months
    months: [18],
    cliff: 0,
    label: '3'
  },
  type4: {
    unlocking: 0, // 0%
    monthly: [0.05], // 5% monthly over 20 months
    months: [20],
    // 4 months represented in milliseconds. Months of 30 days are assumed.
    cliff: VESTING_START_TIMESTAMP.add(4, 'months').diff(VESTING_START_TIMESTAMP, 'seconds'),
    label: '4'
  },
  type5: {
    unlocking: 0, // 0%
    monthly: [0.02, 0.04], // 2% monthly over 10 months, then 4% over 20 months
    months: [10, 20],
    // 4 months represented in milliseconds. Months of 30 days are assumed.
    cliff: VESTING_START_TIMESTAMP.add(4, 'months').diff(VESTING_START_TIMESTAMP, 'seconds'),
    label: '5'
  },
}
const POOLS_SUPPLY: PoolsSupply = {
  preSeed: 2_500_000,
  seed: 54_600_000,
  longVesting: 52_000_000,
  shortVesting: 26_000_000,
  marketing: 48_000_000,
  development: 40_000_000,
  community: 20_000_000,
  teamAndAdvisors: 50_000_000,
  companyReserve: 106_900_000,
}

const ALLOCATION_TOTAL_SUPPLY = 400_000_000;

const ALLOCATIONS: Allocations = {
  preSeed: {
    percentage: parseEther(`${POOLS_SUPPLY.preSeed / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type4
  },
  seed: {
    percentage: parseEther(`${POOLS_SUPPLY.seed / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type2
  },
  marketing: {
    percentage: parseEther(`${POOLS_SUPPLY.marketing / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type1
  },
  longVesting: {
    percentage: parseEther(`${POOLS_SUPPLY.longVesting / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type5
  },
  shortVesting: {
    percentage: parseEther(`${POOLS_SUPPLY.shortVesting / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type2
  },
  development: {
    percentage: parseEther(`${POOLS_SUPPLY.development / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type4
  },
  community: {
    percentage: parseEther(`${POOLS_SUPPLY.community / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type3
  },
  companyReserve: {
    percentage: parseEther(`${POOLS_SUPPLY.companyReserve / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type5
  },
  teamAndAdvisors: {
    percentage: parseEther(`${POOLS_SUPPLY.teamAndAdvisors / ALLOCATION_TOTAL_SUPPLY}`),
    vestingInfo: VESTING_TYPES.type4
  },
}
const ownerAddress = '0xBc67849Ae7A1dc56b457eC4FAA504023f6cBDDb5'

describe('Vesting merkle tree', () => {
  it('Invalid owner address', async (done) => {
    try {
      new VestingTree({
        allocations: ALLOCATIONS,
        balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
        users: [],
        vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
      })
    } catch(error) {
      const message: string = error.message;
      const expect = 'Invalid owner address'

      if(message.startsWith(expect)) {
        return done();
      }

      done(error);
    }
  })

  it(
    'The vestingTree should be generated correctly when users are allocated tokens for a ' +
    'specific amount and also when an address is in more than one pool or even when it is ' +
    'found more than once in the same pool',
    () => {
      const users: VestingUsers[] = [
        {
          address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC", // => DUPLICATE ADDRESS
          allocationsType: "community",
          amount: parseEther('20000000'),
        },
        {
          address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC", // => DUPLICATE ADDRESS
          allocationsType: "companyReserve",
          amount: parseEther('900000'),
        },
        {
          address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462", // => DUPLICATE ADDRESS
          allocationsType: "companyReserve",
          amount: parseEther('100000000'),
        },
        {
          address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462", // => DUPLICATE ADDRESS
          allocationsType: "companyReserve",
          amount: parseEther('6000000'),
        },
        {
          address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD", // => DUPLICATE ADDRESS
          allocationsType: "development",
          amount: parseEther('20000000'),
        },
        {
          address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD", // => DUPLICATE ADDRESS
          allocationsType: "development",
          amount: parseEther('20000000'),
        },
        {
          address: "0xB5B490CB06B0a87232B2F18CB8d9C4646B7e2b95",
          allocationsType: "longVesting",
          amount: parseEther('52000000'),
        },
        {
          address: "0x859Cf27D2d3eCFd17Bd53dB05Da3CdDBe9204277",
          allocationsType: "marketing",
          amount: parseEther('48000000'),
        },
        {
          address: "0x8ee60EBc2AE83462B1888435b2D1389E8410ca61",
          allocationsType: "seed",
          amount: parseEther('54600000'),
        },
        {
          address: "0x312F0B2B93DF482fdA0F25BaD9Af2F0E006eE87F",
          allocationsType: "preSeed",
          amount: parseEther('2500000'),
        },
        {
          address: "0x693D991BB7e1110f52A87356899e82B5c9a21CA1",
          allocationsType: "shortVesting",
          amount: parseEther('26000000'),
        },
        {
          address: "0x75B7AA6E0165977E7C41ead70d2919481bAABe36",
          allocationsType: "teamAndAdvisors",
          amount: parseEther('50000000'),
        }
      ]


      const vestingType = ALLOCATIONS.teamAndAdvisors.vestingInfo as VestingType;

      const treasurers: TreasurerData[] = [
        {
          address: '0x75B7AA6E0165977E7C41ead70d2919481bAABe36',
          vestingType,
        }
      ]

      const tree = new VestingTree({
        users,
        allocations: ALLOCATIONS,
        balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
        vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
        treasurers,
        ownerAddress
      })

      // The methods called below are necessary to meet the coverage requirements.
      tree.hash(tree.vestingSchedules[0]);
      tree.balancehash(BigNumber.from(10));
      tree.treasurerQuantityHash();
      tree.allocationQuantityHash();
  })

  it(
    'The vestringTree should be generated correctly when users are assigned tokens by weight',
    () => {
      const users: VestingUsers[] = [
        {
          address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC",
          allocationsType: "community",
          weight: parseEther('1'),
        },
        {
          address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462",
          allocationsType: "companyReserve",
          weight: parseEther('1'),
        },
        {
          address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD",
          allocationsType: "development",
          weight: parseEther('1'),
        },
        {
          address: "0xB5B490CB06B0a87232B2F18CB8d9C4646B7e2b95",
          allocationsType: "longVesting",
          weight: parseEther('1'),
        },
        {
          address: "0x859Cf27D2d3eCFd17Bd53dB05Da3CdDBe9204277",
          allocationsType: "marketing",
          weight: parseEther('1'),
        },
        {
          address: "0x8ee60EBc2AE83462B1888435b2D1389E8410ca61",
          allocationsType: "seed",
          weight: parseEther('1'),
        },
        {
          address: "0x312F0B2B93DF482fdA0F25BaD9Af2F0E006eE87F",
          allocationsType: "preSeed",
          weight: parseEther('1'),
        },
        {
          address: "0x693D991BB7e1110f52A87356899e82B5c9a21CA1",
          allocationsType: "shortVesting",
          weight: parseEther('1'),
        },
        {
          address: "0x75B7AA6E0165977E7C41ead70d2919481bAABe36",
          allocationsType: "teamAndAdvisors",
          weight: parseEther('1'),
        }
      ]

      new VestingTree({
        users,
        allocations: ALLOCATIONS,
        balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
        vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
        ownerAddress
      })
  })

  it(
    'The vestringTree should be generated correctly when the users are assigned the tokens ' +
    'by a specified amount and also by weight',
    () => {
      const users1: VestingUsers[] = [
        {
          address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC",
          allocationsType: "community",
          amount: parseEther('20000000'),
        },
        {
          address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462",
          allocationsType: "companyReserve",
          amount: parseEther('106900000'),
        },
        {
          address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD",
          allocationsType: "development",
          amount: parseEther('40000000'),
        },
        {
          address: "0xB5B490CB06B0a87232B2F18CB8d9C4646B7e2b95",
          allocationsType: "longVesting",
          weight: parseEther('1'),
        },
        {
          address: "0x859Cf27D2d3eCFd17Bd53dB05Da3CdDBe9204277",
          allocationsType: "marketing",
          weight: parseEther('1'),
        },
        {
          address: "0x8ee60EBc2AE83462B1888435b2D1389E8410ca61",
          allocationsType: "seed",
          weight: parseEther('1'),
        },
        {
          address: "0x312F0B2B93DF482fdA0F25BaD9Af2F0E006eE87F",
          allocationsType: "preSeed",
          weight: parseEther('1'),
        },
        {
          address: "0x693D991BB7e1110f52A87356899e82B5c9a21CA1",
          allocationsType: "shortVesting",
          weight: parseEther('1'),
        },
        {
          address: "0x75B7AA6E0165977E7C41ead70d2919481bAABe36",
          allocationsType: "teamAndAdvisors",
          weight: parseEther('1'),
        }
      ]

      new VestingTree({
        users: users1,
        allocations: ALLOCATIONS,
        balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
        vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
        ownerAddress
      })

      const users2: VestingUsers[] = [
        {
          address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC",
          allocationsType: "community",
          amount: parseEther('10000000'),
        },
        {
          address: "0xaA40a2FfFdFe12F162F983061483103d52C290E8",
          allocationsType: "community",
          weight: parseEther('0.5'),
        },
        {
          address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462",
          allocationsType: "companyReserve",
          amount: parseEther('53450000'),
        },
        {
          address: "0x7BD6295aC45C441c1Cdc637c624b675563a1f902",
          allocationsType: "companyReserve",
          weight: parseEther('0.5'),
        },
        {
          address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD",
          allocationsType: "development",
          amount: parseEther('20000000'),
        },
        {
          address: "0x89BF650e7C3563eE35c2FD58BfEdDf3614212E5C",
          allocationsType: "development",
          weight: parseEther('0.5'),
        },
        {
          address: "0xB5B490CB06B0a87232B2F18CB8d9C4646B7e2b95",
          allocationsType: "longVesting",
          amount: parseEther('26000000'),
        },
        {
          address: "0x034b44B18D5F82e675F932a896EE01DA2bce65D7",
          allocationsType: "longVesting",
          weight: parseEther('0.5'),
        },
        {
          address: "0x859Cf27D2d3eCFd17Bd53dB05Da3CdDBe9204277",
          allocationsType: "marketing",
          amount: parseEther('24000000'),
        },
        {
          address: "0x82d7F64DbDba062139129151381E4a367130f735",
          allocationsType: "marketing",
          weight: parseEther('0.5'),
        },
        {
          address: "0x8ee60EBc2AE83462B1888435b2D1389E8410ca61",
          allocationsType: "seed",
          amount: parseEther('27300000'),
        },
        {
          address: "0x299D26aE93Df3DB7488d6D39650d645b39568100",
          allocationsType: "seed",
          weight: parseEther('0.5'),
        },
        {
          address: "0x312F0B2B93DF482fdA0F25BaD9Af2F0E006eE87F",
          allocationsType: "preSeed",
          amount: parseEther('1250000'),
        },
        {
          address: "0x2F7462b19437e0c880A5C735aC5f559d7535DDC9",
          allocationsType: "preSeed",
          weight: parseEther('0.5'),
        },
        {
          address: "0x693D991BB7e1110f52A87356899e82B5c9a21CA1",
          allocationsType: "shortVesting",
          amount: parseEther('13000000'),
        },
        {
          address: "0x4BfdfEa3e160CaCF66f0fD93Fa7e953174f1F6c4",
          allocationsType: "shortVesting",
          weight: parseEther('0.5'),
        },
        {
          address: "0x75B7AA6E0165977E7C41ead70d2919481bAABe36",
          allocationsType: "teamAndAdvisors",
          amount: parseEther('25000000'),
        },
        {
          address: "0xB5Db7C9C8B7C75347710E99f8b3866C1bA01974C",
          allocationsType: "teamAndAdvisors",
          weight: parseEther('0.5'),
        }
      ]

      new VestingTree({
        users: users2,
        allocations: ALLOCATIONS,
        balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
        vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
        ownerAddress
      })
  })

  it(
    'Verify that the vestingTree is generated correctly when a pool has the vesting type unlocked',
    () => {
      const allocations: Allocations = {
        preSeed: {
          percentage: parseEther(`${POOLS_SUPPLY.preSeed / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: 'unlocked'
        },
        seed: {
          percentage: parseEther(`${POOLS_SUPPLY.seed / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type2
        },
        marketing: {
          percentage: parseEther(`${POOLS_SUPPLY.marketing / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type1
        },
        longVesting: {
          percentage: parseEther(`${POOLS_SUPPLY.longVesting / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type5
        },
        shortVesting: {
          percentage: parseEther(`${POOLS_SUPPLY.shortVesting / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type2
        },
        development: {
          percentage: parseEther(`${POOLS_SUPPLY.development / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type4
        },
        community: {
          percentage: parseEther(`${POOLS_SUPPLY.community / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type3
        },
        companyReserve: {
          percentage: parseEther(`${POOLS_SUPPLY.companyReserve / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type5
        },
        teamAndAdvisors: {
          percentage: parseEther(`${POOLS_SUPPLY.teamAndAdvisors / ALLOCATION_TOTAL_SUPPLY}`),
          vestingInfo: VESTING_TYPES.type4
        },
      }

      const users: VestingUsers[] = [
        {
          address: "0x312F0B2B93DF482fdA0F25BaD9Af2F0E006eE87F",
          allocationsType: "preSeed",
          amount: parseEther('1250000'),
        },
        {
          address: "0x2F7462b19437e0c880A5C735aC5f559d7535DDC9",
          allocationsType: "preSeed",
          weight: parseEther('0.5'),
        },
        {
          address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC",
          allocationsType: "community",
          amount: parseEther('10000000'),
        },
        {
          address: "0xaA40a2FfFdFe12F162F983061483103d52C290E8",
          allocationsType: "community",
          weight: parseEther('0.5'),
        },
        {
          address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462",
          allocationsType: "companyReserve",
          amount: parseEther('53450000'),
        },
        {
          address: "0x7BD6295aC45C441c1Cdc637c624b675563a1f902",
          allocationsType: "companyReserve",
          weight: parseEther('0.5'),
        },
        {
          address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD",
          allocationsType: "development",
          amount: parseEther('20000000'),
        },
        {
          address: "0x89BF650e7C3563eE35c2FD58BfEdDf3614212E5C",
          allocationsType: "development",
          weight: parseEther('0.5'),
        },
        {
          address: "0xB5B490CB06B0a87232B2F18CB8d9C4646B7e2b95",
          allocationsType: "longVesting",
          amount: parseEther('26000000'),
        },
        {
          address: "0x034b44B18D5F82e675F932a896EE01DA2bce65D7",
          allocationsType: "longVesting",
          weight: parseEther('0.5'),
        },
        {
          address: "0x859Cf27D2d3eCFd17Bd53dB05Da3CdDBe9204277",
          allocationsType: "marketing",
          amount: parseEther('24000000'),
        },
        {
          address: "0x82d7F64DbDba062139129151381E4a367130f735",
          allocationsType: "marketing",
          weight: parseEther('0.5'),
        },
        {
          address: "0x8ee60EBc2AE83462B1888435b2D1389E8410ca61",
          allocationsType: "seed",
          amount: parseEther('27300000'),
        },
        {
          address: "0x299D26aE93Df3DB7488d6D39650d645b39568100",
          allocationsType: "seed",
          weight: parseEther('0.5'),
        },
        {
          address: "0x693D991BB7e1110f52A87356899e82B5c9a21CA1",
          allocationsType: "shortVesting",
          amount: parseEther('13000000'),
        },
        {
          address: "0x4BfdfEa3e160CaCF66f0fD93Fa7e953174f1F6c4",
          allocationsType: "shortVesting",
          weight: parseEther('0.5'),
        },
        {
          address: "0x75B7AA6E0165977E7C41ead70d2919481bAABe36",
          allocationsType: "teamAndAdvisors",
          amount: parseEther('25000000'),
        },
        {
          address: "0xB5Db7C9C8B7C75347710E99f8b3866C1bA01974C",
          allocationsType: "teamAndAdvisors",
          weight: parseEther('0.5'),
        }
      ]

      new VestingTree({
        allocations,
        users,
        balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
        vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
        ownerAddress
      })
  })

  it('Get list of all cliffs', () => {
    const ALLOCATIONS: Allocations = {
      preSeed: {
        percentage: parseEther(`${POOLS_SUPPLY.preSeed / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: 'unlocked'
      },
      seed: {
        percentage: parseEther(`${POOLS_SUPPLY.seed / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type2
      },
      marketing: {
        percentage: parseEther(`${POOLS_SUPPLY.marketing / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type1
      },
      longVesting: {
        percentage: parseEther(`${POOLS_SUPPLY.longVesting / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type5
      },
      shortVesting: {
        percentage: parseEther(`${POOLS_SUPPLY.shortVesting / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type2
      },
      development: {
        percentage: parseEther(`${POOLS_SUPPLY.development / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type4
      },
      community: {
        percentage: parseEther(`${POOLS_SUPPLY.community / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type3
      },
      companyReserve: {
        percentage: parseEther(`${POOLS_SUPPLY.companyReserve / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type5
      },
      teamAndAdvisors: {
        percentage: parseEther(`${POOLS_SUPPLY.teamAndAdvisors / ALLOCATION_TOTAL_SUPPLY}`),
        vestingInfo: VESTING_TYPES.type4
      },
    }

    const users: VestingUsers[] = [
      {
        address: "0x312F0B2B93DF482fdA0F25BaD9Af2F0E006eE87F",
        allocationsType: "preSeed",
        amount: parseEther('1250000'),
      },
      {
        address: "0x2F7462b19437e0c880A5C735aC5f559d7535DDC9",
        allocationsType: "preSeed",
        weight: parseEther('0.5'),
      },
      {
        address: "0x5Fd08589bA4Effa9d83667e5f3E407eC2b3C65AC",
        allocationsType: "community",
        amount: parseEther('10000000'),
      },
      {
        address: "0xaA40a2FfFdFe12F162F983061483103d52C290E8",
        allocationsType: "community",
        weight: parseEther('0.5'),
      },
      {
        address: "0x95E94fEA887320e87c834Ec8c260eC9E712F0462",
        allocationsType: "companyReserve",
        amount: parseEther('53450000'),
      },
      {
        address: "0x7BD6295aC45C441c1Cdc637c624b675563a1f902",
        allocationsType: "companyReserve",
        weight: parseEther('0.5'),
      },
      {
        address: "0x6aFc75F986C2F0D293b07E7E737aCe75011C1AfD",
        allocationsType: "development",
        amount: parseEther('20000000'),
      },
      {
        address: "0x89BF650e7C3563eE35c2FD58BfEdDf3614212E5C",
        allocationsType: "development",
        weight: parseEther('0.5'),
      },
      {
        address: "0xB5B490CB06B0a87232B2F18CB8d9C4646B7e2b95",
        allocationsType: "longVesting",
        amount: parseEther('26000000'),
      },
      {
        address: "0x034b44B18D5F82e675F932a896EE01DA2bce65D7",
        allocationsType: "longVesting",
        weight: parseEther('0.5'),
      },
      {
        address: "0x859Cf27D2d3eCFd17Bd53dB05Da3CdDBe9204277",
        allocationsType: "marketing",
        amount: parseEther('24000000'),
      },
      {
        address: "0x82d7F64DbDba062139129151381E4a367130f735",
        allocationsType: "marketing",
        weight: parseEther('0.5'),
      },
      {
        address: "0x8ee60EBc2AE83462B1888435b2D1389E8410ca61",
        allocationsType: "seed",
        amount: parseEther('27300000'),
      },
      {
        address: "0x299D26aE93Df3DB7488d6D39650d645b39568100",
        allocationsType: "seed",
        weight: parseEther('0.5'),
      },
      {
        address: "0x693D991BB7e1110f52A87356899e82B5c9a21CA1",
        allocationsType: "shortVesting",
        amount: parseEther('13000000'),
      },
      {
        address: "0x4BfdfEa3e160CaCF66f0fD93Fa7e953174f1F6c4",
        allocationsType: "shortVesting",
        weight: parseEther('0.5'),
      },
      {
        address: "0x75B7AA6E0165977E7C41ead70d2919481bAABe36",
        allocationsType: "teamAndAdvisors",
        amount: parseEther('25000000'),
      },
      {
        address: "0xB5Db7C9C8B7C75347710E99f8b3866C1bA01974C",
        allocationsType: "teamAndAdvisors",
        weight: parseEther('0.5'),
      }
    ]

    const tree = new VestingTree({
      allocations: ALLOCATIONS,
      users,
      balance: parseEther(ALLOCATION_TOTAL_SUPPLY.toString()),
      vestingStartTimestamp: VESTING_START_TIMESTAMP.unix(),
      ownerAddress
    });

    tree.getCliffs();
  })
})