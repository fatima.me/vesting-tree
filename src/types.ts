import { BigNumber } from "ethers";

export type AllocationsType =
  | 'preSeed'
  | 'seed'
  | 'longVesting'
  | 'shortVesting'
  | 'marketing'
  | 'development'
  | 'community'
  | 'teamAndAdvisors'
  | 'companyReserve';

export type PoolsSupply = Record<AllocationsType, number>;

export interface Allocation {
  percentage: BigNumber;
  vestingInfo: VestingType | 'unlocked';
}

export type Allocations = Record<AllocationsType, Allocation>;

export interface VestingType {
  // percentage represented in floating point numbers
  unlocking: number;
  // monthly release percentage list. The order is important in relation to "months"
  monthly: number[];
  // List of number of months to release. The order is important in relation to "monthly"
  months: number[];
  /*
  * Blocking delay at the time of TGE as seconds since the Unix epoch. This value represents the
  * seconds elapsed from the start of the TGE to the block date. Example: if the TGE starts at
  * 1670193083 `seconds` and a delay of 1 month is desired then the value of cliff should be the
  * number of seconds that must be added to the start date for 1 month to elapse
  **/
  cliff: number;
  label: string;
};

export type VestingTypes = Record<string, VestingType | 'unlocked'>;

export interface VestingUsers {
  weight?: BigNumber; // Percentage that corresponds to the user
  amount?: BigNumber; // Amount of tokens to be awarded. IMPORTANT: if this value is present it must have priority over the weight (%)
  address: string;
  allocationsType: AllocationsType;
}

export type PartialRecord<K extends keyof any, T> = {
  [P in K]?: T;
};

export type GroupedByUsers = Record<string, VestingSchedule[]>;
export type GroupedByCliff = Record<string, VestingSchedule[]>;
export type AllocationTypeMapCliff = PartialRecord<AllocationsType, GroupedByCliff>;

export interface VestingSchedule extends Pick<
  VestingUsers,
  | 'address'
  | 'allocationsType'
> {
  amount: string; // Amount of tokens to be released
  vestingCliff: number; // Lock delay for release
}

export interface TreasurerData {
  address: string;
  vestingType: VestingType;
}

export type TreasurerDataByAddress = Record<string, TreasurerData>;

export interface VestingTreeParams {
  /* Total balance associated with the vesting tree */
  balance: BigNumber;
  /* Users who will be the beneficiaries of the allocations */
  users: VestingUsers[];
  /* Object with the different allocation groups */
  allocations: PartialRecord<AllocationsType, Allocation>;
  /* Timestamp of vesting start as seconds since the Unix epoch */
  vestingStartTimestamp: number;
  /**
   * A treasurer is an address which has the possibility of generating new TGE with the tokens
	 * that are assigned to it at the time of contract deployment.
  **/
  treasurers?: TreasurerData[];
  ownerAddress?: string;
}

export interface VestingScheduleWithProof extends VestingSchedule {
  hash: string;
  proof: string;
  transactionHash?: string;
}

// Information of a user for the dapp
export interface UserDapp {
  totalVesting: string;
  endDateTimestamp: number;
  allocationsType: string;
  vestingSchedules: VestingScheduleWithProof[];
}

export interface verifyDataIntegrityProps {
  balance: BigNumber;
  ownerBalance: BigNumber;
  vestingStartTimestamp: number;
  allocations: PartialRecord<AllocationsType, Allocation>;
}